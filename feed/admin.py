from django.contrib import admin

# Register your models here.
from feed.models import PostModel


class PostAdmin(admin.ModelAdmin):
    pass

admin.site.register(PostModel, PostAdmin)